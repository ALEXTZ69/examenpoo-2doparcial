﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class especificaciones:dispositivodecorador
    {
        public especificaciones (dispositivodecorador orden):base(orden)
        {
            this.orden = orden;
        }

        public override void GenerarSoporte()
        {
            Console.WriteLine("Marca: Apple");
            Console.WriteLine("Modelo: 20 PRO");
            Console.WriteLine("Sistema Operativo: IOS");
            Console.WriteLine("IMEI:012345678901234");
        }
    }
}
