﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reparacion de un Celular");
            celular celular1 = new celular();
            celular1.GenerarSoporte();
            celular1.Pais("China");
            dispositivodecorador decorador1 = new dispositivodecorador(celular1);
            decorador1.GenerarSoporte();
            especificaciones especificaciones1 = new especificaciones(decorador1);
            especificaciones1.GenerarSoporte();
            danos danos1 = new danos(decorador1);
            danos1.GenerarSoporte();
            celular1.Extra(false);

            Console.WriteLine("******************************************");

            Console.WriteLine("Reparacion de una Tablet");
            tablet tablet1 = new tablet();
            tablet1.GenerarSoporte();
            tablet1.ChipyPais(true, "Korea");
            dispositivodecorador decorador2 = new dispositivodecorador(celular1);
            decorador2.GenerarSoporte();
            especificaciones especificaciones2 = new especificaciones(decorador1);
            especificaciones2.GenerarSoporte();
            danos danos2 = new danos(decorador1);
            danos2.GenerarSoporte();


            Console.ReadKey();
        }
    }
}
